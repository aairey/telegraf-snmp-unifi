FROM vincejv/telegraf-snmp

COPY UBNT-FROGFOOT-RESOURCES-MIB.mib \
     UBNT-MIB \
     UBNT-UniFi-MIB \
     /usr/share/snmp/mibs/
